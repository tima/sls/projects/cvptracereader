/**
 * @brief CVP1 Trace Reader
 * Author: Arthur Perais (arthur.perais@gmail.com)
**/

/**
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org>
**/

/**
 * Use this at your own risk, the author is making no guarantee that this is 100% correct
 * Please report any issue on the git repo, thank you !
 */

#pragma once

#include <fstream>
#include <cstring>
#include <iostream>
#include <array>
#include <cassert>
#include <algorithm>
#include <unordered_map>

#include "./gzstream.h"

enum InstClass : uint8_t
{
  aluInstClass = 0,
  loadInstClass = 1,
  storeInstClass = 2,
  condBranchInstClass = 3,
  uncondDirectBranchInstClass = 4,
  uncondIndirectBranchInstClass = 5,
  fpInstClass = 6,
  slowAluInstClass = 7,
  undefInstClass = 8
};

enum AddressingMode : uint8_t
{
  NoMode,
  BaseImmediateOffset,
  BaseRegOffset,
  BaseUpdate,
  DCZVA,
  Prefetch,
  PCRelative
};

static inline constexpr int StackPointer = 31;
static inline constexpr int MAX_SRC = 4;
static inline constexpr int MAX_DST = 4;

struct AddrModeInfo
{
  AddressingMode m_mode = AddressingMode::NoMode;
  uint8_t m_corrected_access_size = 0;
  uint8_t m_base_reg = 0;
  uint64_t m_mask = ~0;
  uint64_t m_last_ea = 0;

  AddrModeInfo() = default;

  AddrModeInfo(AddressingMode mode, uint8_t access_size, uint8_t base_reg, uint64_t mask = ~0)
  : m_mode(mode)
  , m_corrected_access_size(access_size)
  , m_base_reg(base_reg)
  , m_mask(mask)
  {
  }
};


// INT registers are registers 0 to 31. SIMD/FP registers are registers 32 to 63. Flag register is register 64
enum Offset
{
  vecOffset = 32,
  ccOffset = 64,
  invalidOffset = 255
};

// This structure is used by CVP1's simulator.
// Adapt for your own needs.
struct Operand
{
  bool m_valid;
  uint64_t m_value_lo;
  uint64_t m_value_hi;
  uint8_t m_reg_id;
  bool m_base_reg;

  bool valid() const { return m_valid; }
  bool isBaseReg() const { return m_base_reg; }
  uint64_t getValue() const { return m_value_lo; }
  uint64_t getValueHi() const { return m_value_hi; }
  bool isSimd() const { return m_reg_id >= Offset::vecOffset && m_reg_id < Offset::ccOffset; }
  bool isFlags() const { return m_reg_id == Offset::ccOffset; }
  uint8_t getName() const { return m_reg_id; }

  friend std::ostream& operator<<(std::ostream& stream, const Operand& op)
  {
    if(op.valid())
    {
      stream << "[Reg:" << (int) op.getName() << " Val:" << std::hex;
      if(op.isSimd())
      {
        stream << op.getValueHi();
      } 
      stream << op.getValue();
      if(op.isBaseReg())
      {
        stream << " Breg";
      }
      stream << std::dec << "]";
    }
    else
    {
      stream << "[InvalidReg]";
    }
    return stream;
  }
};

// This structure is used by CVP1's simulator.
// Adapt for your own needs.
struct Instruction
{
  uint8_t m_type;
  uint64_t m_pc;
  bool m_taken;
  uint64_t m_next_pc;

  std::array<Operand, MAX_SRC> m_sources;
  std::array<Operand, MAX_DST> m_dests;

  uint64_t m_mem_ea;
  uint8_t m_mem_access_size;
  
  AddressingMode m_addr_mode;

  bool isLoad() const { return m_type == InstClass::loadInstClass; }
  bool isStore() const { return m_type == InstClass::storeInstClass; }
  bool isBranch() const
  { 
    return m_type == InstClass::condBranchInstClass ||
      m_type == InstClass::uncondDirectBranchInstClass ||
      m_type == InstClass::uncondIndirectBranchInstClass;
  }

  int numInRegs()
  {
    return std::count_if(m_sources.begin(), m_sources.end(), [](const auto & in)
    {
      return in.valid();
    });
  }

  uint8_t baseUpdateReg()
  {
    if(auto it = std::find_if(m_sources.begin(), m_sources.end(), [](const auto & in)
    {
      return in.valid() && in.m_base_reg;
    }); it != m_sources.end())
    {
      return it->getName();
    }
    return Offset::invalidOffset;
  }

  int numOutRegs()
  {
    return std::count_if(m_dests.begin(), m_dests.end(), [](const auto & out)
    {
      return out.valid();
    });
  }

  void reset()
  {
    std::memset(this, 0, sizeof(Instruction));
  }
  
  bool is_base_update;
  bool is_guaranteed_base_update;
  bool ignore_hi_lane;
  bool is_kernel;

  friend std::ostream& operator<<(std::ostream& stream, const Instruction & inst)
  {
    static constexpr const char * cInfo[] = {"aluOp", "loadOp", "stOp", "condBrOp", "uncondDirBrOp", "uncondIndBrOp", "fpOp", "slowAluOp" };
    static constexpr const char * addrModeInfo[] = {"NoMode", "BaseImmOffset", "BaseRegOffset", "BaseUpdate", "DCZVA", "Prefetch", "PCRelative" };

    stream << "[PC: 0x" << std::hex << inst.m_pc << std::dec << " type: "  << cInfo[inst.m_type];

    if(inst.isLoad() || inst.isStore())
    {
      stream << " AddrMode:" << addrModeInfo[inst.m_addr_mode] << " EA: 0x" << std::hex << inst.m_mem_ea << std::dec << " MemSize: " << (int) inst.m_mem_access_size << "B";
    }
     
    if(inst.isBranch())
    {
      stream << " Tkn:" << (int) inst.m_taken << " Targ: 0x" << std::hex << inst.m_next_pc << " " << std::dec;
    }
        
    stream << " - Sources : {";
    for(const auto & src : inst.m_sources)
    {
      if(src.valid())
      {
        stream << " " << src;
      }
    }

    stream << " } - Dests : {";
    for(const auto & dst : inst.m_dests)
    {
      if(dst.valid())
      {
        stream << " " << dst;
      }
    }
    
    stream << " }]";
    return stream;
  }
};


struct CVPTraceReader
{
  // This is a cache to help us refine our addressing mode/ pair vs single detection
  std::unordered_map<uint64_t, AddrModeInfo> addr_mode_helper;
  std::array<uint64_t, 32> current_reg_values = {0};

  gz::igzstream * m_input;

  // Two instruction buffer to fix incorrect indirect call target
  // Note that given this implementation, the very last instruction of the trace
  // will never be returned to whoever uses CVPTraceReader

  std::array<Instruction, 2> m_inst_buffer;
  size_t m_current_index = 0;

  uint64_t m_read_instr = 0;
  
  CVPTraceReader(const char * trace_name)
  {
    m_input = new gz::igzstream();
    m_input->open(trace_name, std::ios_base::in | std::ios_base::binary);

    if(m_input->bad() || m_input->fail())
    {
      std::cerr << "Could not open the file" << std::endl;
      exit(1);
    }

    // Have to kickstart buffer
    readTrace();
  }

  ~CVPTraceReader()
  {
    if(m_input)
      delete m_input;

    std::cout  << " Read " << m_read_instr << " instrs " << std::endl;
  }

  Instruction * readInstr()
  {
    if(readTrace())
    {
     return &m_inst_buffer[m_current_index & 0x1];
    }
    else
    {
      return nullptr;
    }
  }

  // Read bytes from the trace and populate a buffer object.
  // Returns true if something was read from the trace, false if we the trace is over.
  bool readTrace()
  {
    // Trace Format :
    // Inst PC 				- 8 bytes
    // Inst Type			- 1 byte
    // If load/storeInst
    //   Effective Address 		- 8 bytes
    //   Access Size (one reg)		- 1 byte
    // If branch
    //   Taken 				- 1 byte
    //   If Taken
    //     Target			- 8 bytes
    // Num Input Regs 			- 1 byte
    // Input Reg Names 			- 1 byte each
    // Num Output Regs 			- 1 byte
    // Output Reg Names			- 1 byte each
    // Output Reg Values
    //   If INT (0 to 31) or FLAG (64) 	- 8 bytes each
    //   If SIMD (32 to 63)		- 16 bytes each

    auto & instr = m_inst_buffer[++m_current_index & 0x1];
    instr.reset();

    m_input->read((char*) &instr.m_pc, sizeof(instr.m_pc));

    if(m_input->eof())
      return false;

    instr.m_next_pc = instr.m_pc + 4;
    m_input->read((char*) &instr.m_type, sizeof(instr.m_type));

    assert(instr.m_type != undefInstClass);

    if(instr.isBranch())
    {
      // Fill everything, then fix indirect call to x30 later if needed
      m_input->read((char*) &instr.m_taken, sizeof(instr.m_taken));
      if(instr.m_taken)
      {
        m_input->read((char*) &instr.m_next_pc, sizeof(instr.m_next_pc));
      }
    }

    if(instr.isLoad() || instr.isStore())
    {
      m_input->read((char*) &instr.m_mem_ea, sizeof(instr.m_mem_ea));
      // We will fix the access size after we know number of inputs/outputs
      m_input->read((char*) &instr.m_mem_access_size, sizeof(instr.m_mem_access_size));
    }

    uint8_t num_in_regs = 0;
    m_input->read((char*) &num_in_regs, sizeof(num_in_regs));

    for(auto i = 0; i != num_in_regs; i++)
    {
      m_input->read((char*) &instr.m_sources[i].m_reg_id, sizeof(instr.m_sources[i].m_reg_id));
      instr.m_sources[i].m_valid = true;
    }

    uint8_t num_out_regs = 0;
    m_input->read((char*) &num_out_regs, sizeof(num_out_regs));

    for(auto i = 0; i != num_out_regs; i++)
    {
      m_input->read((char*) &instr.m_dests[i].m_reg_id, sizeof(instr.m_dests[i].m_reg_id));
      instr.m_dests[i].m_valid = true;
    }

    for(auto i = 0; i != num_out_regs; i++)
    {
      m_input->read((char*) &instr.m_dests[i].m_value_lo, sizeof(instr.m_dests[i].m_value_lo));
      if(instr.m_dests[i].isSimd())
      {
        m_input->read((char*) &instr.m_dests[i].m_value_hi, sizeof(instr.m_dests[i].m_value_hi));
      }
      instr.m_dests[i].m_valid = true;
    }

    // Fix I : Add flag register destination to ALU instructions with zero destinations
    if((instr.m_type == InstClass::aluInstClass || instr.m_type == InstClass::fpInstClass) && num_out_regs == 0)
    {
      instr.m_dests[0].m_reg_id = Offset::ccOffset;
      instr.m_dests[0].m_valid = true;
    }

    // Fix I bis : Add flag register source to cond Branches with zero input
    if(instr.m_type == InstClass::condBranchInstClass && num_in_regs == 0)
    {
      instr.m_sources[0].m_reg_id = Offset::ccOffset;
      instr.m_sources[0].m_valid = true;
    }

    // Fix II : Fix indirect call to x30 target
    auto & other_instr = m_inst_buffer[(m_current_index + 1) & 0x1];
    if(other_instr.m_type == InstClass::uncondIndirectBranchInstClass &&
      other_instr.numInRegs() == 1 && other_instr.numOutRegs() == 1 &&
      other_instr.m_sources[0].getName() == 30)
    {
      other_instr.m_next_pc = instr.m_pc;
    }

    // Fix III : Correct access size
    if(!instr.isLoad() && !instr.isStore() && addr_mode_helper.count(instr.m_pc) != 0)
    {
      // Remove info if instruction is from another thread/process
      addr_mode_helper.erase(instr.m_pc);
    }
    else if((instr.isLoad() || instr.isStore()) && addr_mode_helper.count(instr.m_pc) == 0)
    {
      if(instr.isLoad())
      {
        // Case 1 : Single dest reg
        if(num_out_regs == 1)
        {
          // Case 1.1 : Single src reg
          if(num_in_regs == 1)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
          }
          // Case 1.2 : Two src regs
          else if(num_in_regs == 2)
          { 
            auto outReg = instr.m_dests[0].getName();
            // Case 1.2.1 : Source also in destination but single dest, so it cannot be
            // Base Update. The inst is just overwriting one of the register used to compute the address
            if(auto base_reg = std::find_if(instr.m_sources.begin(), instr.m_sources.end(), [outReg](const auto & in)
            {
              return in.valid() && in.getName() == outReg;
            }); base_reg != instr.m_sources.end())
            {
              addr_mode_helper[instr.m_pc] = {AddressingMode::BaseRegOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
            }
            // Case 1.2.2 : No source is also destination
            else
            {
              addr_mode_helper[instr.m_pc] = {AddressingMode::BaseRegOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
            } 
          }
          // Don't forget PC-relative
          else if(num_in_regs == 0)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::PCRelative, instr.m_mem_access_size, 0};
          }
        } 
        // Case 2 : Two dest regs
        else if(num_out_regs == 2)
        {
          if(num_in_regs == 1)
          {
            // Case 2.1 : Single src reg also dest reg
            if(auto base_reg = std::find_if(instr.m_dests.begin(), instr.m_dests.end(), [&](const auto & out)
            {
              return out.valid() && out.getName() == instr.m_sources[0].getName();
            }); base_reg != instr.m_dests.end())
            {
              // For regular load/store : "Pre/Post indexed by an unscaled 9-bit signed immediate offset" hence -256 to 255 immediate
              auto base_reg_value = base_reg->getValue();
              const bool fits_unscaled_baseupdate_immediate = 
                (base_reg_value >= instr.m_mem_ea && ((base_reg_value - instr.m_mem_ea) <= 256)) ||
                (base_reg_value < instr.m_mem_ea && ((instr.m_mem_ea - base_reg_value) <= 255));

              // Case 2.1.1 : Value of dest reg (base reg) is within base update immediate, this is regular load 
              // with BaseUpdate addressing
              // Note that this is not bulletproof e.g. if it is actually ldp reg1, reg2, [reg1] and we have bad luck
              if(fits_unscaled_baseupdate_immediate)
              {
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, instr.m_mem_access_size, instr.m_sources[0].getName()};
              }
              // Case 2.1.2 : Value of dest reg (base reg) is not within base update immediate, this is load pair
              else
              {
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[0].getName()};
              }
            }
            // Case 2.2 : Single src reg not also dest reg
            else
            {
              addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[0].getName()};
            }
          }
          else
          {
            assert(num_in_regs == 2); // Two sources

            // LD* with the register post index
            auto base_reg_index = 0;
            for(base_reg_index = 0; base_reg_index < num_in_regs; base_reg_index++)
            {
              if(instr.m_sources[base_reg_index].getName() == instr.m_dests[0].getName())
              {
                break;
              }
            }

            assert (base_reg_index < num_in_regs);
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, instr.m_mem_access_size, instr.m_dests[0].getName()};
          }
        }
        // Case 3 : Three dest regs, load pair or SIMD load multiple with base update
        else if(num_out_regs >= 3)
        {
          // We assume that the base register (if any) will be the first destination
          // so make sure that there is a single one (at least, non SIMD)
          auto num_integer_dest_regs = 0;
          for(int i = 0; i < num_out_regs; i++)
          {
            if(!instr.m_dests[i].isSimd())
            {
              num_integer_dest_regs ++;
            }
          }
          assert(num_in_regs == 1 || num_integer_dest_regs == 1);
          bool base_update = false;

          for(int i = 0; i < num_in_regs; i++)
          {
            if(instr.m_sources[i].getName() == instr.m_dests[0].getName())
            {
              base_update = true;
            }
          }

          if(base_update)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, (uint8_t) (instr.m_mem_access_size * (num_out_regs - 1)), instr.m_dests[0].getName()};
          }
          else
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size * num_out_regs), instr.m_sources[0].getName()};
          }
          addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, (uint8_t) (instr.m_mem_access_size * (num_out_regs - 1)), instr.m_sources[0].getName()};
        }
        // Case 4 : Load with zero dest regs (= prefetch)
        else if(num_out_regs == 0)
        {
          addr_mode_helper[instr.m_pc] = {AddressingMode::Prefetch, instr.m_mem_access_size, instr.m_sources[0].getName()};
        }
      }
      else 
      {
        assert(instr.isStore());

        // Case 1 : One source
        if(num_in_regs == 1)
        {
          // Case 1.1 DCZVA, have to align EA to 64B because
          // "There are no alignment restrictions on this address." and traces were captured with 64B cache line size
          if(instr.m_mem_access_size == 64)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::DCZVA, instr.m_mem_access_size, instr.m_sources[0].getName(), ~63lu};
          }
          // Case 1.2 Specific ops
          else
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
          }
        }
        // Case 2 : Two sources
        else if(num_in_regs == 2)
        {
          const bool has_base_update = std::find_if(instr.m_sources.begin(), instr.m_sources.end(), [&](const auto & in)
          {
            return in.valid() && in.getName() == instr.m_dests[0].getName();
          }) != instr.m_sources.end();

          // Case 2.1 : No destination registers
          if(num_out_regs == 0)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
          }
          // Case 2.2 : One dest reg, differs from two sources (strex?)
          else if(num_out_regs == 1 && !has_base_update)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, instr.m_mem_access_size, instr.m_sources[0].getName()};
          }
          // Case 2.3 : One dest reg, same as one of the sources
          else if(has_base_update)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, instr.m_mem_access_size, instr.m_sources[0].getName()};
          }
        }
        // Case 3 : Three sources or more (SIMD store multiple or STP)
        else if(num_in_regs >= 3)
        {
          // The first non SIMD reg is the base register
          int base_reg_idx = 0;
          for(auto it = instr.m_sources.begin(); it != instr.m_sources.end(); ++it)
          {
            if(it->valid() && !it->isSimd())
              break;
            base_reg_idx++;
          }

          assert(base_reg_idx != instr.numInRegs());

          // Case 3.2 : No outputs
          if(num_out_regs == 0)
          {
            auto base_reg_value = current_reg_values[instr.m_sources[base_reg_idx].getName()];

            // Can't really make a decision here, wait until next time
            if(base_reg_value == 0x0)
            {
              addr_mode_helper[instr.m_pc] = {AddressingMode::NoMode, instr.m_mem_access_size, instr.m_sources[base_reg_idx].getName()};
            }
            else
            {
              // For store pair : 
              // "Pre-indexed by a scaled 7-bit signed immediate offset."
              // "Post-indexed by a scaled 7-bit signed immediate offset."
              const int scaling_factor = instr.m_mem_access_size;
              const bool fits_stp_immediate_offset = 
                ((instr.m_mem_ea >= base_reg_value) && ((instr.m_mem_ea - base_reg_value) <= ((uint64_t) (63 * scaling_factor)))) ||
                ((instr.m_mem_ea < base_reg_value) && ((base_reg_value - instr.m_mem_ea) <= ((uint64_t) (64 * scaling_factor))));

              // Case 3.2.1 : store pair BaseImmediateOffset if value of base reg is within immediate offset of EA
              // Note that this is not bulletproof
              if(fits_stp_immediate_offset)
              {
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[base_reg_idx].getName()};
              }
              // Case 3.2.2 : regular store using two registers for address
              else
              {
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseRegOffset, instr.m_mem_access_size, instr.m_sources[base_reg_idx].getName()};
              }
            }
          }
          else
          {
            // Case 3.1: >=3 sources, 1 destination
            if(num_out_regs == 1)
            {
              if((instr.m_dests[0].getValue() == 0) || (instr.m_dests[0].getValue() == 1))
              {
                // Case 3.1.1: Output value is 0/1 then it is (most probably) STXP (Store Exclusive Pair writes 0/1 to indicate sucess or no write performed)
                // The addressing mode is BaseRegImmediate, but because it is a pair, the data transfer size is 2x.
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[base_reg_idx].getName()};
              }
              else
              {
                // Case 3.1.2: Output value is not 0/1 then it is Store Pair with Base update
                addr_mode_helper[instr.m_pc] = {AddressingMode::BaseUpdate, (uint8_t) (instr.m_mem_access_size  * (num_in_regs - 1)), instr.m_dests[0].getName()};
              }
            }

            // Case 3.2: >=3 sources, 2 destinations
            else
            {
              // Make sure source registers are not SIMD... otherwise it could be ST4?
              for(int i = 0; i < num_in_regs; i++)
              {
                if(instr.m_sources[i].isSimd())
                {
                  assert(false);
                }
              }

              // Looks like Compare and Swap Pair, which reads into two consecutive regs, compares with two consecutive regs, and stores two consecutive regs. 
              // That's pretty specific... and not base update
              addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size * num_in_regs), instr.m_sources[base_reg_idx].getName()};
            }  
          }
        }
      }
    }
               
    if(instr.isLoad() || instr.isStore())
    {
      auto info = addr_mode_helper.find(instr.m_pc);
      assert(info != addr_mode_helper.end());

      // Attempt to fix non categorized stores with three sources (Case 3.2)
      if(info->second.m_mode == AddressingMode::NoMode)
      {
        auto base_reg_value = current_reg_values[instr.m_sources[info->second.m_base_reg].getName()];
        const bool ea_valid = info->second.m_last_ea != 0x0;
        const bool same_ea = info->second.m_last_ea == instr.m_mem_ea;
        // Address is the same a last time
        // This is three source store with single base register, hence store pair
        if(ea_valid && same_ea)
        {
          addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[info->second.m_base_reg].getName()};
        }
        // Base reg did not change but EA changed, so it's using a second register for address
        else if(ea_valid && !same_ea && base_reg_value == 0x0)
        {
          addr_mode_helper[instr.m_pc] = {AddressingMode::BaseRegOffset, instr.m_mem_access_size, instr.m_sources[info->second.m_base_reg].getName()};
        }
        // Base reg did change (it had to be 0x0 before otherwise would have categorized it)
        // which caused EA to change since last time
        // We have to redo the check for case 3.2.1 vs 3.2.2
        else if(ea_valid && !same_ea && base_reg_value != 0x0)
        {
          // For store pair : 
          // "Pre-indexed by a scaled 7-bit signed immediate offset."
          // "Post-indexed by a scaled 7-bit signed immediate offset."
          const int scaling_factor = instr.m_mem_access_size;
          const bool fits_stp_immediate_offset = 
            ((instr.m_mem_ea > base_reg_value) && ((instr.m_mem_ea - base_reg_value) <= ((uint64_t) (63 * scaling_factor)))) ||
            ((instr.m_mem_ea < base_reg_value) && ((base_reg_value - instr.m_mem_ea) <= ((uint64_t) (64 * scaling_factor))));

          // Case 3.2.1 : store pair BaseImmediateOffset if value of base reg is within immediate offset of EA
          // Note that this is not bulletproof
          if(fits_stp_immediate_offset)
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseImmediateOffset, (uint8_t) (instr.m_mem_access_size << 1), instr.m_sources[info->second.m_base_reg].getName()};
          }
          // Case 3.2.2 : regular store using two registers for address
          else
          {
            addr_mode_helper[instr.m_pc] = {AddressingMode::BaseRegOffset, instr.m_mem_access_size, instr.m_sources[info->second.m_base_reg].getName()};
          }
        }
      }
       
      instr.m_mem_access_size = info->second.m_corrected_access_size;
      instr.m_addr_mode = info->second.m_mode;

      // To align address for DCZVA
      instr.m_mem_ea &= info->second.m_mask;
      info->second.m_last_ea = instr.m_mem_ea;

      for(auto & src : instr.m_sources)
      {
        if(src.getName() == info->second.m_base_reg)
        {
          src.m_base_reg = true;
        }
      }

      // Marking dest reg as base reg is only useful to control
      // its latency in a timing simulator (base reg is available after
      // ALU cycle, not after load to use cycle)
      // So, only mark it if it is an actual BaseUpdate
      // and not just BaseReg or BaseImm that happens to load
      // data in the base register
      if(instr.m_addr_mode == AddressingMode::BaseUpdate)
      {
        for(auto & dst : instr.m_dests)
        {
          if(dst.getName() == info->second.m_base_reg)
          {
            dst.m_base_reg = true;
          }
        }
      }
    }

    // Updating current register value to help
    // with store addr mode disambiguation
    for(const auto & out : instr.m_dests)
    {
      if(out.valid() && !out.isSimd() && !out.isFlags())
      {
        current_reg_values[out.getName()] = out.getValue();
      }
    }

    m_read_instr++;

    if(m_read_instr % 100000 == 0)
      std::cout << std::dec << m_read_instr << " instrs " << std::endl;

    return true;
  }
};
