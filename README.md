# Description

This is a trace reader that will take a CVP trace as input and read the instructions, applying a couple of fixes on the way.
This code can be used as a base for using CVP traces in your tool. Ideally, such tool could be used to regenerate fixed traces from original CVP-1 traces.

# Content

- `./cvp_trace_reader.h` is the trace reader code itself.
- `./gzstream.h` and `./gzstream.C` are external code under LGPL to allow streaming from gzip-compressed files. See [https://www.cs.unc.edu/Research/compgeom/gzstream/](https://www.cs.unc.edu/Research/compgeom/gzstream/) for details. If you do not want to use LGPL code, consider porting the reader code to use the [Shrinkwrap](https://github.com/jonathonl/shrinkwrap) library (MIT License).
- `./main.cpp` is a small example that just prints instructions read from a trace.

# Compiling the Example and Using

```
g++ gzstream.cc main.cpp -o reader -Wall -std=c++17 -DGZSTREAM_NAMESPACE=gz -lz -O3
./reader trace.gz
```

# Trace Format

The traces contain ARMv8 instructions that have been stripped from information to anonymize the traces. Each instruction is encoded in the trace using the following binary format :

|                    Field                   |                     Size                     |                                                            Comment                                                           |
|:------------------------------------------:|:--------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------:|
|                     PC                     |                    8 bytes                   | PC only (no ASID/EL), virtual address                                                                                        |
| InstClass                                  |                    1 byte                    | -                                                                                                                            |
| Effective Memory Address (Load/Store only) |                    8 bytes                   | Virtual address                                                                                                              |
| Access size (Load/Store only)              |                    1 byte                    | Access size of a single register. If instruction loads multiple registers, multiply by that number.                          |
| Branch direction (branch only)             |                    1 byte                    | Also present for unconditional branches                                                                                      |
| Branch Target (taken branch only)          |                    8 bytes                   | Virtual address. Incorrect for indirect calls that jump to x30 (Armv8-A link register), **needs to be fixed by the reader**. |
| Number of input registers                  |                    1 byte                    |                                                                                                                              |
| Identifier of input registers              |                  1 byte each                 |                                                                                                                              |
| Number of output registers                 |                    1 byte                    |                                                                                                                              |
| Identifier of output registers             |                  1 byte each                 |                                                                                                                              |
| Value of output registers                  | 8 bytes if general purpose, 16 bytes if SIMD |                                                                                                                              |

## InstClass 

```
enum InstClass : uint8_t
{
  aluInstClass = 0,
  loadInstClass = 1,
  storeInstClass = 2,
  condBranchInstClass = 3,
  uncondDirectBranchInstClass = 4,
  uncondIndirectBranchInstClass = 5,
  fpInstClass = 6,
  slowAluInstClass = 7,
  undefInstClass = 8
};
```

## Register Identifiers

| Identifiers | Register Type   |
|:-----------:|:---------------:|
| 0 to 31     | General Purpose |
| 32 to 63    | SIMD            |
| 64          | Flags (NZVC)    |


## Missing/Incorrect Information

As the traces have been anonymized, important information has been removed, which prevents the traces from being useful in all cases. The trace reader attempts to infer back some of the missing information.

### Missing Flag Register Source/Destination

Since the flag register is not a general purpose register, it is not present as a source/destination register in the trace. To properly establish dependencies, the tracer adds the flag register as the destination of all integer instructions with zero destination registers, and as source of all conditional branches. This is slightly pessimistic as syscall instructions will be marked as generating the flags, but those are fairly rare.

### Indirect Call to ARMv8 Link Register (x30)

The target of indirect calls to x30 (identified in the trace as `uncondIndirectbranchInstClass`, source register `30`, destination register `30`) is incorrect in the traces, as it is always the PC of the call instruction + 4 (that is, the return address stored in the link register).
The trace reader corrects this by keeping a window of two instructions, and overwriting the target of such indirect calls to x30 with the PC of the next instruction from the trace. Note that this fix will fail if the trace goes into an interrupt handler or exception exactly at the indirect call to x30.

### Memory Access Size and Addressing Mode

A first issue is that the trace only contains the transfer size, which is the memory access size for one source or destination register. For instance, `ldr x2, [x3, #8]!` has two destinations registers (`2` and `3`), but does a single memory access, hence the true memory access size is the one encoded in the trace, `8`, times the number of registers getting data from memory, hence `8`. Conversely, `ldp x2, x3, [x4]` also has two destinations registers (`2` and `3` as well), but it does two memory accesses, and so the true access size is the one encoded in the trace, `8`, times the numbers of registers getting data from memory, hence `16`. A similar issue exists for store instructions. 

A second issue is that the trace does not contain the addressing mode, which is problematic as ARMv8 features a pre/post-increment addressing mode that updates the base register used to compute the effective address with an immediate as part of the memory instruction. For instance, `ldr x2, [x3, #8]!` will load into `x2` the content of memory location `x3 + 8`, but it will also update `x3` with `x3 + 8`. Thus, the instructions has two destination registers, but only `x2` gets data from memory. As a result, there is no reason for the base register update to have the same execution latency as the memory access. In other words, in a timing simulator, `x3` should be available after a single cycle (or the ALU latency) while `x2` should be available after the memory hierarchy access latency. 

To address the two issues, the trace reader attempts to infer the addressing mode of load/store instructions, Note that this inference is best effort as it is not always possible to get the exact answer. This allows to differentiate regular load/store from load pair/store pair, for which the true memory access size is 2x the one encoded in the trace. This also allows to mark the destination register that is the base address register as such for memory instructions using `BaseUpdate` addressing, so that a timing simulator can apply the correct latencies.

The following heuristic (also commented in trace reader) is used :

#### Loads

- **Case 1** : If a load has a single destination register
  - **Case 1.1** : Load has single source register, then the load is using `BaseImmediateOffset`. **True access size = 1 x trace access size**.
  - **Case 1.2** : Load has two source registers
    - **Case 1.2.1** : Destination register is among source registers, then load is using `BaseRegOffset` addressing and happens to overwrite one of the two source regs. This is **not** base update addressing. **True access size = 1 x trace access size**.
    - **Case 1.2.2** : Destination register is **not** among source registers, then load is using `BaseRegOffset` addressing. **True access size = 1 x trace access size**.
- **Case 2** : If a load has two destinations registers
  - **Case 2.1** : Load has a single source register that is also a destination register
    - **Case 2.1.1** : If the value of said source/destination register is within `[EA+minImmOffset;EA+maxImmOffset]`, then it is *likely* using `BaseUpdate` addressing. **True access size = 1 x trace access size**.
    - **Case 2.2.1** : If the value of said source/destination register is **not** within `[EA+minImmOffset;EA+maxImmOffset]`, then this is *likely* a load pair using `BaseImmediateOffset` addressing. **True access size = 2 x trace access size**.
  - **Case 2.2** : If the source register is not also a destination register, then this is a load pair using `BaseImmediateOffset` addressing. **True access size = 2 x trace access size**.
- **Case 3** : If a load has three destination registers, then the load is a load pair and uses `BaseUpdate` addressing. **True access size = 2 x trace access size**.
- **Case 4** : If a load has zero destination registers, then it is a prefetch instruction and is marked as such by using `Prefetch` "addressing mode".

#### Stores

- **Case 1** : If a store has a single source register.
  - **Case 1.1** If access size is 64B, it is DCZVA (zeroing cache line). Note that no alignment is required for the address, hence we have to fix the address of the first byte so this does not become a cache line crossing access. Addressing mode is marked as `DCZVA` in the trace reader. **True access size = 1 x trace access size**
  - **Case 1.2** : If a store has a single source register and not 64B access size, then we mark it as `BaseImmediateOffset`. **True access size = 1 x trace access size**.
- **Case 2** : If a store has two source registers, it is either
  - **Case 2.1** : No destination register, the store is `BaseImmediateOffset`. **True access size = 1 x trace access size**.
  - **Case 2.2** : Single destination register that is not also a source, the store is `BaseImmediateOffset`. **True access size = 1 x trace access size**.
  - **Case 2.3** : Single destination register that is also a source, the store is `BaseUpdate`. **True access size = 1 x trace access size**.
- **Case 3**: If a store has three source registers
  - **Case 3.1** : Store has one destination that is also in the sources, it is `BaseUpdate` store pair. **True access size = 2 x trace access size**.
  - **Case 3.2** : Store has no destination
    - **Case 3.2.1** : If value of base reg is within `[EA+minImmOffset;EA+maxImmOffset]`, this is a store pair using `BaseImmediateOffset`. **True access size = 2 x trace access size**.
    - **Case 3.2.2** : If value of base reg is **not** within `[EA+minImmOffset;EA+maxImmOffset]`, this is a regular store `BaseRegOffset`. **True access size = 1 x trace access size**.

### Instruction Categorization

The trace makes obvious efforts to categorize instructions broadly. This makes finding privilege boundaries hard, because syscall/sysreturn and exceptions are not marked. This reader does not improve on this particular aspect, but a general heuristic would be to mark any `aluOp` whose next instruction is not `PC + 4` as a syscall or sysreturn (at least a privilege changing instruction).